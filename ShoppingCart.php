<?php

namespace Apple\iTunes\Checkout;

class ShoppingCart {
  protected $items = [];

  public function add(LineItem $lineItem)
  {
    $this->items[] = $lineItem; // array push
  }

  public function getTotal()
  {
    $total = 0;

    foreach($this->items as $lineItem) {
      $total += $lineItem->getTotal();
    }

    return $total;
  }
}
