<?php

namespace Apple\iTunes\Checkout;

class LineItem {
  public $product, $quantity;

  public function __construct($product, int $quantity)
  {
    $this->product = $product;
    $this->quantity = $quantity;
  }

  public function getTotal()
  {
    // quantity * product price
    return $this->quantity * $this->product->price;
  }
}
